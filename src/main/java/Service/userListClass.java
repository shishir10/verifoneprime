/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import beans.users;
import com.shishir.verifoneprime.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shishir
 */
public class userListClass {
    public static ArrayList<users> getUsers(){
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from users");
            ArrayList<users> al = new ArrayList<users>();
            ResultSet rs = ps.executeQuery();
            boolean fetched = false;
            while (rs.next()){
                users us = new users();
                us.setUserName(rs.getString(1));
                us.setUserPassword(rs.getString(2));
                us.setFirstName(rs.getString(3));
                us.setLastName(rs.getString(4));
                us.setEmail(rs.getString(5));
                us.setDepartment(rs.getString(6));
                us.setAccessRole(rs.getString(7));
                us.setRegisterDate(rs.getDate(8));
                us.setLastSeen(rs.getDate(9));
                al.add(us);
                fetched = true;
            }
            rs.close();
            if (fetched)
                return al;
            else
                return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(userListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(userListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } finally {
            Database.close(con);
        }
    }
}

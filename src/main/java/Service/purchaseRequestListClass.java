/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import beans.purchaseRequest;
import com.shishir.verifoneprime.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shishir
 */
public class purchaseRequestListClass {
    public static ArrayList<purchaseRequest> getPurchaseRequest(){
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement("select *from purchase_requisition");
            ResultSet rs = ps.executeQuery();
            ArrayList<purchaseRequest> al = new ArrayList<purchaseRequest>();
            boolean found = false;
            while(rs.next()){
                purchaseRequest pr = new purchaseRequest();
                pr.setPrNumber(rs.getInt(1));
                pr.setDateSubmitted(rs.getDate(2));
                pr.setTotalAmount(rs.getDouble(3));
                pr.setEntity(rs.getString(4));
                pr.setOperatingUnit(rs.getString(5));
                pr.setProject(rs.getString(6));
                pr.setSourceName(rs.getString(7));
                pr.setSourceContact(rs.getString(8));
                pr.setDepartment(rs.getString(9));
                pr.setAccount(rs.getString(10));
                pr.setAttachmentLink(rs.getString(11));
                pr.setCreatedBy(rs.getString(12));
                pr.setCreatedDate(rs.getDate(13));
                pr.setModifiedBy(rs.getString(14));
                pr.setModifiedDate(rs.getDate(15));
                al.add(pr);
                found = true;
            }
            rs.close();
            if (found)
                return al;
            else
                return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(purchaseRequestListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(purchaseRequestListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } finally {
            Database.close(con);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import beans.statementOfWork;
import com.shishir.verifoneprime.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shishir
 */
public class statementOfWorkListClass {
    public static ArrayList<statementOfWork> getstatementOfWork(){
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from statement_of_work");
            ResultSet rs = ps.executeQuery();
            boolean found = false;
            ArrayList<statementOfWork> al = new ArrayList<statementOfWork>();
            while (rs.next()) {
                statementOfWork sow = new statementOfWork();
                sow.setPrNumber(rs.getInt(1));
                sow.setVendorName(rs.getString(2));
                sow.setEmployeeName(rs.getString(3));
                sow.setStartDate(rs.getDate(4));
                sow.setEndDate(rs.getDate(5));
                sow.setAmount(rs.getDouble(6));
                sow.setCreatedBy(rs.getString(7));
                sow.setCreatedDate(rs.getDate(8));
                sow.setModifiedBy(rs.getString(9));
                sow.setModifiedDate(rs.getDate(10));
                al.add(sow);
                found = true;
            }
            rs.close();
            if (found)
                return al;
            else
                return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(statementOfWorkListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(statementOfWorkListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        }
    }
}

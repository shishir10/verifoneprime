/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import beans.purchaseOffer;
import com.shishir.verifoneprime.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shishir
 */
public class purchaseOfferListClass {
    public static ArrayList<purchaseOffer> getPurchaseOffer() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from purchase_offer");
            ResultSet rs = ps.executeQuery();
            ArrayList<purchaseOffer> al = new ArrayList<purchaseOffer>();
            boolean found = false;
            while (rs.next()){
                purchaseOffer po = new purchaseOffer();
                po.setPrNumber(rs.getInt(1));
                po.setPurchaseAmount(rs.getDouble(2));
                po.setPurchaseDate(rs.getDate(3));
                po.setEmployeeName(rs.getString(4));
                po.setDuration(rs.getShort(5));
                po.setVendorName(rs.getString(6));
                po.setAttachmentLink(rs.getString(7));
                po.setCreatedBy(rs.getString(8));
                po.setCreatedDate(rs.getDate(9));
                po.setModifiedBy(rs.getString(10));
                po.setModifiedDate(rs.getString(11));
                al.add(po);
                found = true;
            }
            rs.close();
            if (found)
                return al;
            else
                return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(purchaseOfferListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(purchaseOfferListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } finally {
            Database.close(con);
        }
    }
}

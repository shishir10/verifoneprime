/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import beans.vendors;
import com.shishir.verifoneprime.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shishir
 */
public class vendorListClass {
    public static ArrayList<vendors> getVendors() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from vendors");
            ArrayList<vendors> al = new ArrayList<vendors>();
            ResultSet rs = ps.executeQuery();
            boolean found = false;
            while (rs.next()){
                vendors v = new vendors();
                v.setVendorName(rs.getString(1));
                v.setContactInfo(rs.getString(2));
                v.setCreatedBy(rs.getString(3));
                v.setCreatedDate(rs.getDate(4));
                v.setModifiedBy(rs.getString(5));
                v.setModifiedDate(rs.getDate(6));
                al.add(v);
                found = true;
            }
            rs.close();
            if (found)
                return al;
            else
                return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(vendorListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(vendorListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } finally {
            Database.close(con);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import beans.employee;
import com.shishir.verifoneprime.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shishir
 */
public class employeeListClass {
    public static ArrayList<employee> getEmployees() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement("select * from employee");
            ResultSet rs = ps.executeQuery();
            boolean found = false;
            ArrayList<employee> al = new ArrayList<employee>();
            while (rs.next()) {
                employee e = new employee();
                e.setPrNumber(rs.getInt(1));
                e.setFirstName(rs.getString(2));
                e.setLastName(rs.getString(3));
                e.setExperience(rs.getByte(4));
                e.setCtc(rs.getDouble(5));
                e.setImageLink(rs.getString(6));
                e.setCreatedBy(rs.getString(7));
                e.setCreatedDate(rs.getDate(8));
                e.setModifiedBy(rs.getString(9));
                e.setModifiedDate(rs.getDate(10));
                al.add(e);
                found = true;
            }
            rs.close();
            if (found)
                return al;
            else
                return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(employeeListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(employeeListClass.class.getName()).log(Level.SEVERE, null, ex);
            return (null);
        } finally {
            Database.close(con);
        }
    }
}

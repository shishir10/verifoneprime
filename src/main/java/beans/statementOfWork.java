/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import Service.statementOfWorkListClass;
import com.shishir.verifoneprime.Database;
import com.shishir.verifoneprime.Util;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author shishir
 */
public class statementOfWork implements Serializable {

    /**
     * Creates a new instance of statementOfWork
     */
    public statementOfWork() {
    }

    private int prNumber;
    private String vendorName;
    private String employeeName;
    private Date startDate;
    private Date endDate;
    private Double amount;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private ArrayList<statementOfWork> list;
    private statementOfWork selectedws;
    private Map<String, String> prn;
    private Map<String, String> ename;
    private Map<String, String> vname;

    @PostConstruct
    public void init() {
        prn = new HashMap<String, String>();
        ename = new HashMap<String, String>();
        vname = new HashMap<String, String>();
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement("select pr_number, employee_name, vendor_name "
                    + "from purchase_offer where "
                    + "pr_number NOT IN (select pr_number from statement_of_work)");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                prn.put(String.valueOf(rs.getInt(1)), String.valueOf(rs.getInt(1)));
                ename.put(String.valueOf(rs.getInt(1)), rs.getString(2));
                vname.put(String.valueOf(rs.getInt(1)), rs.getString(3));
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(statementOfWork.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(statementOfWork.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Database.close(con);
        }
    }

    public Map<String, String> getPrn() {
        return prn;
    }

    public Map<String, String> getEname() {
        return ename;
    }

    public Map<String, String> getVname() {
        return vname;
    }

    public statementOfWork getSelectedws() {
        return selectedws;
    }

    public void setSelectedws(statementOfWork selectedws) {
        this.selectedws = selectedws;
    }

    public ArrayList<statementOfWork> getList() {
        return statementOfWorkListClass.getstatementOfWork();
    }

    public int getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(int prNumber) {
        this.prNumber = prNumber;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
    
    
    public void onPrChange() {
        if (prNumber != 0) {
            this.employeeName = ename.get(String.valueOf(prNumber));
            this.vendorName = vname.get(String.valueOf(prNumber));
        } else {
            this.employeeName = null;
            this.vendorName = null;
        }
    }
    
    public void onDateChange() {
        if (startDate != null) {
        }
    }
    

    public String createNew() {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            Statement st = con.createStatement();
            this.setCreatedBy(Util.getUserName());
            this.setCreatedDate(java.sql.Date.valueOf(LocalDate.now()));
            int i = st.executeUpdate("insert into statement_of_work (pr_number, vendor_name, employee_name, start_date, "
                    + "end_date, amount, created_by, created_date) values ('" + prNumber + "', '" + vendorName + "', '"
                    + employeeName + "', '" + startDate + "', '" + endDate + "', '" + amount + "', '" + createdBy + "', '"
                    + createdDate + "')");
            if (i > 0) {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Data saved successfully", "Success");
                cont.addMessage("Success", facesMessage1);
                return "statementofwork";
            } else {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", "Failiure");
                cont.addMessage("Error", facesMessage1);
                return (null);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(statementOfWork.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", "Failiure");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(statementOfWork.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", "Failiure");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } finally {
            Database.close(con);
        }
    }

    public void delete(statementOfWork var) {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            Statement st;
            st = con.createStatement();
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1;
            int l = st.executeUpdate("delete from statement_of_work where pr_number = '" + var.prNumber + "'");
            if (l > 0) {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "1 Record Successfully deleted", var.prNumber + " Deleted");
                cont.addMessage("msg", facesMessage1);
            } else {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Something went wrong", "Error");
                cont.addMessage("msg", facesMessage1);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Class not found", "Error");
            cont.addMessage("Error", facesMessage1);
        } catch (SQLException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "SQL problem", "Error");
            cont.addMessage("Error", facesMessage1);
        } finally {
            Database.close(con);
        }
    }

}

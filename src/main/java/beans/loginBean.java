/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import com.shishir.verifoneprime.UserDAO;
import com.shishir.verifoneprime.Util;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author shishir
 */
public class loginBean implements Serializable {

    /**
     * Creates a new instance of loginBean
     */
    public loginBean() {
    }

    private String username;
    private String passwd;
    private String role = null;
    private boolean superuser;
    private String fname;
    private String lname;
    private String department;

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getDepartment() {
        return department;
    }

    public boolean isSuperuser() {
        return superuser;
    }

    public void setSuperuser(boolean superuser) {
        this.superuser = superuser;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String loginApp() {
        String pass = String.valueOf(passwd.hashCode());
        boolean result = UserDAO.login(username, pass);
        if (result) {
            this.role = UserDAO.getRole(username, pass);
            this.fname = UserDAO.getFname(username, pass);
            this.lname = UserDAO.getlname(username, pass);
            this.department = UserDAO.getDep(username, pass);
            HttpSession session = Util.getSession();
            session.setAttribute("userName", username);
            session.setAttribute("loggenIn", true);
            session.setAttribute("userRole", role);
            this.superuser = this.role.equalsIgnoreCase("admin");
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Hello " + username, "Welcome Back !");
            cont.addMessage("msg", facesMessage1);
            return "home";
        } else {
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid login credentials " + result, "Try Again !!");
            cont.addMessage("msg", facesMessage1);
            return "login";
        }
    }
    
    public String logout() {
        HttpSession session = Util.getSession();
        session.invalidate();
        return "login";
    }
    
//    public boolean isSuperuser() {
//        return role.equalsIgnoreCase("admin");
//    }

}

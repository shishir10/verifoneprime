/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import Service.userListClass;
import com.shishir.verifoneprime.Database;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author shishir
 */
public class users implements Serializable {

    /**
     * Creates a new instance of users
     */
    public users() {
    }

    public users(String userName, String userPassword, String firstName, String lastName, String email, String department, String accessRole, Date registerDate, Date lastSeen) {
        this.userName = userName;
        this.userPassword = userPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.department = department;
        this.accessRole = accessRole;
        this.registerDate = registerDate;
        this.lastSeen = lastSeen;
    }

    private String userName;
    private String userPassword;
    private String firstName;
    private String lastName;
    private String email;
    private String department;
    private String accessRole;
    private Date registerDate;
    private Date lastSeen;
    private ArrayList<users> list;
    private users selectedUser;

    public users getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(users selectedUser) {
        this.selectedUser = selectedUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getAccessRole() {
        return accessRole;
    }

    public void setAccessRole(String accessRole) {
        this.accessRole = accessRole;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String createUser() {
        Connection con1 = null;
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            Statement st1 = con1.createStatement();
            String hashp = String.valueOf(userPassword.hashCode());
            ResultSet rs;
            rs = st1.executeQuery("select * from users");
            while (rs.next()) {
                if (rs.getString("user_name").equalsIgnoreCase(userName)) {
                    FacesContext cont = FacesContext.getCurrentInstance();
                    FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "User Name already exists", "Try Again !!");
                    cont.addMessage("Error", facesMessage1);
                    return null;
                }
            }
            int i = st1.executeUpdate("insert into users(user_name, user_password, first_name, last_name, email, department, "
                    + "access_role) values ('" + userName + "','"
                    + hashp + "','" + firstName + "','" + lastName + "','"
                    + email + "','" + department + "','" + accessRole + "')");
            if (i > 0) {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Data saved successfully", "Success");
                cont.addMessage("msg", facesMessage1);
                return "users";
            }else {
                return null;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The Driver class could not be loaded", "Failed");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "The querey didn't complete properly", "Failed");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } finally {
            Database.close(con1);
        }
    }

    public ArrayList<users> getList() {
        return userListClass.getUsers();
    }

    public void delete(users var) {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            Statement st;
            st = con.createStatement();
            int i = st.executeUpdate("delete from users where user_name = '" + var.userName + "'");
            st.close();
            if (i > 0) {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Record Successfully deleted", var.userName + " Deleted");
                cont.addMessage("msg", facesMessage1);
            } else {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Something went wrong", "Error");
                cont.addMessage("msg", facesMessage1);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Class not found", "Error");
            cont.addMessage("Error", facesMessage1);
        } catch (SQLException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "SQL problem", "Error");
            cont.addMessage("Error", facesMessage1);
        } finally {
            Database.close(con);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import Service.vendorListClass;
import com.shishir.verifoneprime.Database;
import com.shishir.verifoneprime.Util;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author shishir
 */
public class vendors implements Serializable {

    /**
     * Creates a new instance of vendors
     */
    public vendors() {
    }

    private String vendorName;
    private String contactInfo;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private ArrayList<vendors> list;
    private vendors selectedVendor;

    public vendors getSelectedVendor() {
        return selectedVendor;
    }

    public void setSelectedVendor(vendors selectedVendor) {
        this.selectedVendor = selectedVendor;
    }

    public ArrayList<vendors> getList() {
        return vendorListClass.getVendors();
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String createNew() {
        Connection con1 = null;
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            Statement st1 = con1.createStatement();
            String userName = Util.getUserName();
            java.sql.Date today = java.sql.Date.valueOf(LocalDate.now());
            ResultSet rs;
            rs = st1.executeQuery("select * from vendors");
            while (rs.next()) {
                if (rs.getString("vendor_name").equalsIgnoreCase(this.vendorName)) {
                    FacesContext cont = FacesContext.getCurrentInstance();
                    FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Vendor Name already exists", "Try Again !!");
                    cont.addMessage("Error", facesMessage1);
                    return null;
                }
            }
            int i = st1.executeUpdate("insert into vendors (vendor_name, contact_info, created_by, created_date)"
                    + " values ('" + vendorName + "', '" + contactInfo + "', '" + userName + "', '" + today + "')");
            if (i > 0) {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Data saved successfully", "Success");
                cont.addMessage("msg", facesMessage1);
                return "vendors";
            } else {
                return null;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The Driver class could not be loaded", "Failed");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "The querey didn't complete properly", "Failed");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } finally {
            Database.close(con1);
        }
    }

    public void delete(vendors var) {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            Statement st;
            st = con.createStatement();
            int i = st.executeUpdate("delete from vendors where vendor_name = '" + var.vendorName + "'");
            st.close();
            if (i > 0) {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Record Successfully deleted", var.vendorName + " Deleted");
                cont.addMessage("msg", facesMessage1);
            } else {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Something went wrong", "Error");
                cont.addMessage("msg", facesMessage1);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Class not found", "Error");
            cont.addMessage("Error", facesMessage1);
        } catch (SQLException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "SQL problem", "Error");
            cont.addMessage("Error", facesMessage1);
        } finally {
            Database.close(con);
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import Service.purchaseOfferListClass;
import com.shishir.verifoneprime.Database;
import com.shishir.verifoneprime.Util;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author shishir
 */
public class purchaseOffer implements Serializable {

    /**
     * Creates a new instance of purchaseOffer
     */
    public purchaseOffer() {
    }

    private int prNumber;
    private Double purchaseAmount;
    private Date purchaseDate;
    private String employeeName;
    private int duration;
    private String vendorName;
    private String attachmentLink;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private String modifiedDate;
    private ArrayList<purchaseOffer> list;
    private purchaseOffer selectedpo;
    private Map<String, String> ventmp;
    private Map<String, String> emptmp;
    private Map<String, String> prn;

    public Map<String, String> getVentmp() {
        return ventmp;
    }

    public Map<String, String> getEmptmp() {
        return emptmp;
    }

    public Map<String, String> getPrn() {
        return prn;
    }

    @PostConstruct
    public void init() {
        emptmp = new HashMap<String, String>();
        ventmp = new HashMap<String, String>();
        prn = new HashMap<String, String>();
//        ven = new HashMap<String, Map<String, String>>();
//        emp = new HashMap<String, Map<String, String>>();
//        prdata = new HashMap<String, Map<String, Map<String, String>>>();
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement("select pr_number, first_name, last_name from "
                    + "employee where pr_number NOT IN (select pr_number from purchase_offer)");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
//                emptmp = new HashMap<String, String>();
//                ventmp = new HashMap<String, String>();
                PreparedStatement ps1 = con.prepareStatement("select source_name from purchase_requisition "
                        + "where pr_number = '" + rs.getInt(1) + "'");
                ResultSet rs1 = ps1.executeQuery();
                while (rs1.next()) {
                    ventmp.put(String.valueOf(rs.getInt(1)), rs1.getString(1));
                }
                emptmp.put(String.valueOf(rs.getInt(1)), rs.getString(2) + " " + rs.getString(3));
                prn.put(String.valueOf(rs.getInt(1)), String.valueOf(rs.getInt(1)));
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(purchaseOffer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(purchaseOffer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Database.close(con);
        }
    }

    public purchaseOffer getSelectedpo() {
        return selectedpo;
    }

    public void setSelectedpo(purchaseOffer selectedpo) {
        this.selectedpo = selectedpo;
    }

    public ArrayList<purchaseOffer> getList() {
        return purchaseOfferListClass.getPurchaseOffer();
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public int getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(int prNumber) {
        this.prNumber = prNumber;
    }

    public Double getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setPurchaseAmount(Double purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getAttachmentLink() {
        return attachmentLink;
    }

    public void setAttachmentLink(String attachmentLink) {
        this.attachmentLink = attachmentLink;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void onPrChange() {
        if (prNumber != 0) {
            this.employeeName = emptmp.get(String.valueOf(prNumber));
            this.vendorName = ventmp.get(String.valueOf(prNumber));
        } else {
            employeeName = null;
            vendorName = null;
        }
    }

    public String createNew() {
        Connection con1 = null;
        Statement st1;
        String userName = Util.getUserName();
        java.sql.Date today = java.sql.Date.valueOf(LocalDate.now());
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            st1 = con1.createStatement();
            int i;
            i = st1.executeUpdate("insert into purchase_offer (pr_number, purchase_amount, purchase_date, employee_name, duration, "
                    + "vendor_name, attachment_link, created_by, created_date)"
                    + " values ('" + prNumber + "', '" + purchaseAmount + "', '" + purchaseDate + "', '" + employeeName + "', '" + duration + "', '"
                    + vendorName + "', '" + attachmentLink + "', '" + userName + "', '" + today + "')");
            if (i > 0) {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Data saved successfully", "Success");
                cont.addMessage("Success", facesMessage1);
                return "purchaseoffer";
            } else {
                return null;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(vendors.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The Driver class could not be loaded", "Failed");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(vendors.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The SQL Request could no be completed", "Failed");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } finally {
            Database.close(con1);
        }
    }

    public void delete(purchaseOffer var) {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            Statement st;
            st = con.createStatement();
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1;
//            int k = st.executeUpdate("delete from purchase_offer where pr_number = '" + var.prNumber + "'");
//            if (k > 0) {
//                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "1 Record Successfully deleted", var.prNumber + " Deleted");
//                cont.addMessage("msg", facesMessage1);
//                int l = st.executeUpdate("delete from statement_of_work where pr_number = '" + var.prNumber + "'");
//                if (l > 0) {
//                    facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "2 Records Successfully deleted", var.prNumber + " Deleted");
//                    cont.addMessage("msg", facesMessage1);
//                }
//            } else {
//                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Something went wrong", "Error");
//                cont.addMessage("msg", facesMessage1);
//            }
            int l = st.executeUpdate("delete from statement_of_work where pr_number = '" + var.prNumber + "'");
            int k = st.executeUpdate("delete from purchase_offer where pr_number = '" + var.prNumber + "'");
            if (l > 0 && k <= 0) {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "2 Records Successfully deleted", var.prNumber + " Deleted");
                cont.addMessage("msg", facesMessage1);
            } else if (l > 0 && k > 0) {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "1 Record Successfully deleted", var.prNumber + " Deleted");
                cont.addMessage("msg", facesMessage1);
            } else {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Something went wrong", "Error");
                cont.addMessage("msg", facesMessage1);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Class not found", "Error");
            cont.addMessage("Error", facesMessage1);
        } catch (SQLException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "SQL problem", "Error");
            cont.addMessage("Error", facesMessage1);
        } finally {
            Database.close(con);
        }
    }

}

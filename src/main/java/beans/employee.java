/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import Service.employeeListClass;
import com.shishir.verifoneprime.Database;
import com.shishir.verifoneprime.Util;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author shishir
 */
public class employee implements Serializable {

    /**
     * Creates a new instance of employee
     */
    public employee() {
    }

    private int prNumber;
    private String firstName;
    private String lastName;
    private int experience;
    private Double ctc;
    private String imageLink;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private ArrayList<employee> list;
    private employee selectedemployee;
    private Map<String, String> prn;
    
    @PostConstruct
    public void init() {
        prn = new HashMap<String, String>();
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            PreparedStatement ps = con.prepareStatement("select pr_number from purchase_requisition where pr_number "
                    + "NOT IN (select pr_number from employee)");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                prn.put(rs.getString(1), rs.getString(1));
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(employee.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(employee.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Database.close(con);
        }
    }

    public employee getSelectedemployee() {
        return selectedemployee;
    }

    public Map<String, String> getPrn() {
        return prn;
    }

    public void setSelectedemployee(employee selectedemployee) {
        this.selectedemployee = selectedemployee;
    }

    public ArrayList<employee> getList() {
        return employeeListClass.getEmployees();
    }

    public int getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(int prNumber) {
        this.prNumber = prNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public Double getCtc() {
        return ctc;
    }

    public void setCtc(Double ctc) {
        this.ctc = ctc;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
    
    

    public String createNew() {
        Connection con1 = null;
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            Statement st1 = con1.createStatement();
            String userName = Util.getUserName();
            java.sql.Date today = java.sql.Date.valueOf(LocalDate.now());
            int i;
            i = st1.executeUpdate("insert into employee (pr_number, first_name, last_name, experience, "
                    + "ctc, image_link, created_by, created_date) values('"
                    + prNumber + "', '" + firstName + "', '" + lastName + "', '" + experience + "', '"
                    + ctc + "', '" + imageLink + "', '" + userName + "', '" + today + "')");
            if (i > 0) {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Data saved successfully", "Success");
                cont.addMessage("Success", facesMessage1);
                return "employee";
            } else {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", "Failiure");
                cont.addMessage("Error", facesMessage1);
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(employee.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", "Failiure");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(employee.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", "Failiure");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } finally {
            Database.close(con1);
        }

    }

    public void delete(employee var) {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            Statement st;
            st = con.createStatement();
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1;
//            int j = st.executeUpdate("delete from employee where pr_number = '" + var.prNumber + "'");
//            if (j > 0) {
//                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "1 Record Successfully deleted", var.prNumber + " Deleted");
//                cont.addMessage("msg", facesMessage1);
//                int k = st.executeUpdate("delete from purchase_offer where pr_number = '" + var.prNumber + "'");
//                if (k > 0) {
//                    facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "2 Records Successfully deleted", var.prNumber + " Deleted");
//                    cont.addMessage("msg", facesMessage1);
//                    int l = st.executeUpdate("delete from statement_of_work where pr_number = '" + var.prNumber + "'");
//                    if (l > 0) {
//                        facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "3 Records Successfully deleted", var.prNumber + " Deleted");
//                        cont.addMessage("msg", facesMessage1);
//                    }
//                }
//            } else {
//                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Something went wrong", "Error");
//                cont.addMessage("msg", facesMessage1);
//            }
            int l = st.executeUpdate("delete from statement_of_work where pr_number = '" + var.prNumber + "'");
            int k = st.executeUpdate("delete from purchase_offer where pr_number = '" + var.prNumber + "'");
            int j = st.executeUpdate("delete from employee where pr_number = '" + var.prNumber + "'");
            if (k > 0 && j > 0 && l <= 0) {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "3 Records Successfully deleted", var.prNumber + " Deleted");
                cont.addMessage("msg", facesMessage1);
            }
            if (j > 0 && k > 0 && l <= 0) {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "2 Records Successfully deleted", var.prNumber + " Deleted");
                cont.addMessage("msg", facesMessage1);
            }
            if (j > 0 && k <= 0 && l <= 0) {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "1 Record Successfully deleted", var.prNumber + " Deleted");
                cont.addMessage("msg", facesMessage1);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Class not found", "Error");
            cont.addMessage("Error", facesMessage1);
        } catch (SQLException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "SQL problem", "Error");
            cont.addMessage("Error", facesMessage1);
        } finally {
            Database.close(con);
        }
    }
}

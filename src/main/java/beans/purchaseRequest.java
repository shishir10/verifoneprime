/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import Service.purchaseRequestListClass;
import com.shishir.verifoneprime.Database;
import com.shishir.verifoneprime.Util;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

/**
 *
 * @author shishir
 */
public class purchaseRequest implements Serializable {

    /**
     * Creates a new instance of purchaseRequest
     */
    public purchaseRequest() {
    }

    private int prNumber;
    private Date dateSubmitted;
    private Double totalAmount;
    private String entity;
    private String operatingUnit;
    private String project;
    private String sourceName;
    private String sourceContact;
    private String department;
    private String account;
    private String attachmentLink;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;
    private ArrayList<purchaseRequest> list;
    private purchaseRequest selectedpr;
    private Map<String, String> vnames;
    private Map<String, String> vcontacts;
    private Map<String, Map<String, String>> vmap;

    @PostConstruct
    public void init() {
//        try {
            vnames = new HashMap<String, String>();
            vcontacts = new HashMap<String, String>();
            vmap = new HashMap<String, Map<String, String>>();
            Connection con = null;
            try {
                Class.forName("org.postgresql.Driver");
                con = Database.getConnection();
                PreparedStatement ps = con.prepareStatement("select * from vendors");
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    vnames.put(rs.getString(1), rs.getString(1));
                    vcontacts.put(rs.getString(2), rs.getString(2));
                    vmap.put(rs.getString(1), vcontacts);
                    vcontacts = new HashMap<String, String>();
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(purchaseRequest.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(purchaseRequest.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                Database.close(con);
            }
//            Email email = new SimpleEmail();
//            email.setHostName("smtp.googlemail.com");
//            email.setSmtpPort(465);
//            email.setAuthenticator(new DefaultAuthenticator("shish.1994@gmail.com", "8109995501"));
//            email.setSSLOnConnect(true);
//            try {
//                email.setFrom("admin@gmail.com");
//            } catch (EmailException ex) {
//                Logger.getLogger(purchaseRequest.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            email.setSubject("TestMail");
//            email.setMsg("This is a test mail ... :-) generated via web app :P");
//            email.addTo("shishir@bhangrarocks.com");
//            email.send();
//        } catch (EmailException ex) {
//            Logger.getLogger(purchaseRequest.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    public Map<String, String> getVnames() {
        return vnames;
    }

    public Map<String, String> getVcontacts() {
        return vcontacts;
    }

    public Map<String, Map<String, String>> getVmap() {
        return vmap;
    }

    public purchaseRequest getSelectedpr() {
        return selectedpr;
    }

    public void setSelectedpr(purchaseRequest selectedpr) {
        this.selectedpr = selectedpr;
    }

    public ArrayList<purchaseRequest> getList() {
        return purchaseRequestListClass.getPurchaseRequest();
    }

    public int getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(int prNumber) {
        this.prNumber = prNumber;
    }

    public Date getDateSubmitted() {
        return dateSubmitted;
    }

    public void setDateSubmitted(Date dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getOperatingUnit() {
        return operatingUnit;
    }

    public void setOperatingUnit(String operatingUnit) {
        this.operatingUnit = operatingUnit;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSourceContact() {
        return sourceContact;
    }

    public void setSourceContact(String sourceContact) {
        this.sourceContact = sourceContact;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAttachmentLink() {
        return attachmentLink;
    }

    public void setAttachmentLink(String attachmentLink) {
        this.attachmentLink = attachmentLink;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public void onVendorChange() {
        if (sourceName != null) {
            vcontacts = vmap.get(sourceName);
        } else {
            vcontacts = new HashMap<String, String>();
        }
    }

    public String createNew() {
        Connection con1 = null;
        Statement st1;
        String userName = Util.getUserName();
        java.sql.Date today = java.sql.Date.valueOf(LocalDate.now());
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            st1 = con1.createStatement();
            ResultSet rs;
            rs = st1.executeQuery("select * from purchase_requisition");
            while (rs.next()) {
                if (String.valueOf(rs.getInt("pr_number")).equalsIgnoreCase(String.valueOf(prNumber))) {
                    FacesContext cont = FacesContext.getCurrentInstance();
                    FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Purchase Request already"
                            + " Exists", "Try Again !!");
                    cont.addMessage("Error", facesMessage1);
                    return null;
                }
            }
            int i;
            i = st1.executeUpdate("insert into purchase_requisition (pr_number, date_submitted, total_amount, entity, operating_unit, "
                    + "project, source_name, source_contact, department, account, attachment_link, created_by, created_date)"
                    + " values ('" + prNumber + "', '" + dateSubmitted + "', '" + totalAmount + "', '" + entity + "', '" + operatingUnit + "', '"
                    + project + "', '" + sourceName + "', '" + sourceContact + "', '" + department + "', '" + account + "', '"
                    + attachmentLink + "', '" + userName + "', '" + today + "')");
            if (i > 0) {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Data saved successfully", "Success");
                cont.addMessage("Success", facesMessage1);
                return "purchaserequest";
            } else {
                return null;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(vendors.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The Driver class could not be loaded", "Failed");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(vendors.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The SQL Request could no be completed", "Failed");
            cont.addMessage("Error", facesMessage1);
            return (null);
        } finally {
            Database.close(con1);
        }
    }

    public void createNewHome() {
        Connection con1 = null;
        Statement st1;
        String userName = Util.getUserName();
        java.sql.Date today = java.sql.Date.valueOf(LocalDate.now());
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            st1 = con1.createStatement();
            ResultSet rs;
            rs = st1.executeQuery("select * from purchase_requisition");
            while (rs.next()) {
                if (String.valueOf(rs.getInt("pr_number")).equalsIgnoreCase(String.valueOf(prNumber))) {
                    FacesContext cont = FacesContext.getCurrentInstance();
                    FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Purchase Request already"
                            + " Exists", "Try Again !!");
                    cont.addMessage("Error", facesMessage1);
//                    return null;
                }
            }
            int i;
            i = st1.executeUpdate("insert into purchase_requisition (pr_number, date_submitted, total_amount, entity, operating_unit, "
                    + "project, source_name, source_contact, department, account, attachment_link, created_by, created_date)"
                    + " values ('" + prNumber + "', '" + dateSubmitted + "', '" + totalAmount + "', '" + entity + "', '" + operatingUnit + "', '"
                    + project + "', '" + sourceName + "', '" + sourceContact + "', '" + department + "', '" + account + "', '"
                    + attachmentLink + "', '" + userName + "', '" + today + "')");
            if (i > 0) {
                FacesContext cont = FacesContext.getCurrentInstance();
                FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "Data saved successfully", "Success");
                cont.addMessage("Success", facesMessage1);
//                return "purchaserequest";
            } else {
//                return null;
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(vendors.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The Driver class could not be loaded", "Failed");
            cont.addMessage("Error", facesMessage1);
//            return (null);
        } catch (SQLException ex) {
            Logger.getLogger(vendors.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_ERROR, "The SQL Request could no be completed", "Failed");
            cont.addMessage("Error", facesMessage1);
//            return (null);
        } finally {
            Database.close(con1);
        }
    }

    public void delete(purchaseRequest var) {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = Database.getConnection();
            Statement st;
            st = con.createStatement();
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1;
//            int i = st.executeUpdate("delete from purchase_requisition where pr_number = '" + var.prNumber + "'");
//            if (i > 0) {
//                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "1 Record Successfully deleted", var.prNumber + " Deleted");
//                cont.addMessage("msg", facesMessage1);
//                int j = st.executeUpdate("delete from employee where pr_number = '" + var.prNumber + "'");
//                if (j > 0) {
//                    facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "2 Records Successfully deleted", var.prNumber + " Deleted");
//                    cont.addMessage("msg", facesMessage1);
//                    int k = st.executeUpdate("delete from purchase_offer where pr_number = '" + var.prNumber + "'");
//                    if (k > 0) {
//                        facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "3 Records Successfully deleted", var.prNumber + " Deleted");
//                        cont.addMessage("msg", facesMessage1);
//                        int l = st.executeUpdate("delete from statement_of_work where pr_number = '" + var.prNumber + "'");
//                        if (l > 0) {
//                            facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "4 Records Successfully deleted", var.prNumber + " Deleted");
//                            cont.addMessage("msg", facesMessage1);
//                        }
//                    }
//                }
//            } else {
//                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Something went wrong", "Error");
//                cont.addMessage("msg", facesMessage1);
//            }
            int l = st.executeUpdate("delete from statement_of_work where pr_number = '" + var.prNumber + "'");
            int k = st.executeUpdate("delete from purchase_offer where pr_number = '" + var.prNumber + "'");
            int j = st.executeUpdate("delete from employee where pr_number = '" + var.prNumber + "'");
            int i = st.executeUpdate("delete from purchase_requisition where pr_number = '" + var.prNumber + "'");
            if (l > 0 && k > 0 && j > 0 && i > 0) {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "4 Records Successfully deleted", var.prNumber + " Deleted");
                cont.addMessage("msg", facesMessage1);
            }
            if (k > 0 && j > 0 && i > 0 && l <= 0) {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "3 Records Successfully deleted", var.prNumber + " Deleted");
                cont.addMessage("msg", facesMessage1);
            }
            if (j > 0 && i > 0 && k <= 0 && l <= 0) {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "2 Records Successfully deleted", var.prNumber + " Deleted");
                cont.addMessage("msg", facesMessage1);
            }
            if (i > 0 && j <= 0 && k <= 0 && l <= 0) {
                facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_INFO, "1 Record Successfully deleted", var.prNumber + " Deleted");
                cont.addMessage("msg", facesMessage1);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Class not found", "Error");
            cont.addMessage("Error", facesMessage1);
        } catch (SQLException ex) {
            Logger.getLogger(users.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext cont = FacesContext.getCurrentInstance();
            FacesMessage facesMessage1 = new FacesMessage(FacesMessage.SEVERITY_FATAL, "SQL problem", "Error");
            cont.addMessage("Error", facesMessage1);
        } finally {
            Database.close(con);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listService;

/**
 *
 * @author shishir
 */
public class menuService {

    /**
     * Creates a new instance of menuService
     */
    public menuService() {
    }
    public String home() {
        return "home";
    }
    public String users () {
        return "users";
    }
    public String newUser() {
        return "newuser";
    }
    public String vendors() {
        return "vendors";
    }
    public String purchaseRequest() {
        return "purchaserequest";
    }
    public String employee() {
        return "employee";
    }
    public String purchaseOffer() {
        return "purchaseoffer";
    }
    public String statementofwork() {
        return "statementofwork";
    }
    public String newdata() {
        return "new";
    }
}

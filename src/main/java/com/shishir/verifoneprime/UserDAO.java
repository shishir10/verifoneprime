/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shishir.verifoneprime;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author shishir
 */
public class UserDAO {
    public static boolean login(String user, String Pass){
        Connection con1 = null;
        Statement st1;
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            st1 = con1.createStatement();
            ResultSet rs1;
            rs1 = st1.executeQuery("select user_name, user_password from users where user_name='"
                    + user + "' and user_password='"
                    + Pass + "'");
            if (rs1.next()){
                System.out.println(rs1.getString(1));
                return true;
            }
            else
                return false;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            Database.close(con1);
        }
        
    }
    public static String getRole(String user, String Pass) {
        Connection con1 = null;
        Statement st1;
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            st1 = con1.createStatement();
            ResultSet rs1;
            rs1 = st1.executeQuery("select user_name, user_password, access_role from users where user_name='"
                    + user + "' and user_password='"
                    + Pass + "'");
            if (rs1.next()){
                return rs1.getString(3);
            }
            else
                return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            Database.close(con1);
        }
    }
    public static String getFname(String user, String Pass) {
        Connection con1 = null;
        Statement st1;
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            st1 = con1.createStatement();
            ResultSet rs1;
            rs1 = st1.executeQuery("select user_name, user_password, first_name from users where user_name='"
                    + user + "' and user_password='"
                    + Pass + "'");
            if (rs1.next()){
                return rs1.getString(3);
            }
            else
                return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            Database.close(con1);
        }
    }
    public static String getlname(String user, String Pass) {
        Connection con1 = null;
        Statement st1;
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            st1 = con1.createStatement();
            ResultSet rs1;
            rs1 = st1.executeQuery("select user_name, user_password, last_name from users where user_name='"
                    + user + "' and user_password='"
                    + Pass + "'");
            if (rs1.next()){
                return rs1.getString(3);
            }
            else
                return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            Database.close(con1);
        }
    }
    public static String getDep(String user, String Pass) {
        Connection con1 = null;
        Statement st1;
        try {
            Class.forName("org.postgresql.Driver");
            con1 = Database.getConnection();
            st1 = con1.createStatement();
            ResultSet rs1;
            rs1 = st1.executeQuery("select user_name, user_password, department from users where user_name='"
                    + user + "' and user_password='"
                    + Pass + "'");
            if (rs1.next()){
                return rs1.getString(3);
            }
            else
                return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } finally {
            Database.close(con1);
        }
    }
}

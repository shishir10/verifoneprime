/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shishir.verifoneprime;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author shishir
 */
public class Util {
    
    public static HttpSession getSession() {
        return (HttpSession)
                FacesContext.
                        getCurrentInstance().
                        getExternalContext().
                        getSession(false);
    }
    
    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) 
                FacesContext.
                        getCurrentInstance().
                        getExternalContext().
                        getRequest();
    }
    
    public static String getUserName(){
        HttpSession session = (HttpSession) FacesContext.
                getCurrentInstance().
                getExternalContext().
                getSession(false);
       return session.getAttribute("userName").toString();
    }
    public static String getRole() {
        HttpSession session = getSession();
        if (session != null)
            return (String) session.getAttribute("userRole");
        else
            return null;
    }
}
